'use strict';

require('babel-polyfill');
require('babel-register');

const app = require('./src/server/server');

app.default.listen(4000);

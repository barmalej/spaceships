// @flow
import type { Action } from '../../lib';

export type AppState = {
  heading: string,
};

const initialState = {
  heading: 'Hello morda',
};

const appReducer = (state: AppState = initialState, action: Action) => {
  switch (action.type) {
    case 'UPDATE_HEADING':
      return { ...state, heading: action.payload };
    default:
      return state;
  }
};

export default appReducer;

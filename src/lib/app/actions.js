export const updateHeading = heading => ({
  type: 'UPDATE_HEADING',
  payload: heading,
});

// @flow
import type { Ship, Field, ShipType, Direction } from './reducer';

export type Bound = 'Right' | 'Left' | 'Top' | 'Bottom';

export const shipOnField = (ships: Array<Ship>, field: Field): ?Ship =>
  ships.find(ship => ship.fields.find(f => f.x === field.x && f.y === field.y));

export const updateField = (
  fields: Array<Array<Field>>,
  field: Field,
  newField: Field,
): Array<Array<Field>> => {
  const findField = (f: Field) => f.x === field.x && f.y === field.y;
  const col: ?Array<Field> = fields.find(col => col.find(findField));
  if (!col) return fields;
  const colIndex = fields.indexOf(col);
  const f = col.find(findField);
  const index = col.indexOf(f);
  return [
    ...fields.slice(0, colIndex),
    [...col.slice(0, index), newField, ...col.slice(index + 1)],
    ...fields.slice(colIndex + 1),
  ];
};

export const buildShipFields = (
  shipType: ShipType,
  field: Field,
  direction: Direction,
): Array<Field> => {
  const horizontal = direction === 'W' || direction === 'E';
  const northAngle = direction === 'W' || direction === 'N';

  const shifted = horizontal ? 'x' : 'y';
  const constant = horizontal ? 'y' : 'x';

  const shift = northAngle
    ? coord => field[coord] + 1
    : coord => field[coord] - 1;
  const unshift = northAngle
    ? coord => field[coord] - 1
    : coord => field[coord] + 1;

  const getMainLine = () => [
    { [shifted]: shift(shifted), [constant]: field[constant] },
    field,
    { [shifted]: unshift(shifted), [constant]: field[constant] },
  ];

  switch (shipType) {
    case 'L':
      return [
        ...getMainLine(),
        {
          [shifted]: unshift(shifted),
          [constant]: horizontal ? shift(constant) : unshift(constant),
        },
      ];
    case 'I':
      return getMainLine();
    default:
      return [field];
  }
};

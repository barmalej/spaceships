// @flow
import type { Action } from '../../lib';
import { updateField } from './utils';

export type Field = {|
  x: number,
  y: number,
  missed: boolean,
|};

export type ShipType = 'L' | 'I' | '.';

export type Ship = {
  fields: Array<Field>,
  type: ShipType,
  complete: boolean,
  sunk: boolean,
};

export type GameAction =
  | {| type: 'START_GAME' |}
  | {| type: 'STOP_GAME' |}
  | {| type: 'TOGGLE_DIRECTION' |}
  | {| type: 'ON_MOUSE_OVER' |}
  | {| type: 'ADD_SHIP' |}
  | {| type: 'COLLISION_DETECTED' |}
  | {| type: 'RESET_COLLISION' |}
  | {| type: 'TOGGLE_GAME_OVER_DIALOG' |}
  | {| type: 'SUCCESS', payload: { shootedShip: Ship } |}
  | {| type: 'MISSED', payload: { field: Field } |}
  | {| type: 'FIELD_MOUSE_OVER', payload: { ship: Ship } |}
  | {| type: 'ON_MOUSE_OVER', payload: { field: Field } |};

export type Player = {
  id: number,
  score: number,
  ships: Array<Ship>,
  ready: boolean,
};

export type Direction = 'S' | 'W' | 'N' | 'E';

export type AddShipsMode = { direction: Direction, ship: ?Ship };

export type GameState = {
  started: boolean,
  fields: Array<Array<Field>>,
  awaitingPlayer: Player,
  currentPlayer: Player,
  addShipsMode: ?AddShipsMode,
  showGameOverDialog: boolean,
  showStopGameDialog: boolean,
  collision: boolean,
};

const genFields: Function = (): Array<Array<Field>> => {
  const fields = [];
  /* eslint-disable no-plusplus */
  for (let x = 0; x < 10; x++) {
    const row = [];
    for (let y = 0; y < 10; y++) {
      row.push({ x, y, missed: false });
    }
    fields.push(row);
  }
  return fields;
  /* eslint-enable */
};

export const shipTypes: Array<ShipType> = ['L', 'I', '.', '.'];

const defaultShip: Ship = {
  type: 'L',
  fields: [],
  complete: false,
  sunk: false,
};

const defaultPlayer: Player = {
  id: 1,
  score: 0,
  ships: shipTypes.map(shipType => ({ ...defaultShip, type: shipType })),
  ready: false,
};

const initialState: GameState = {
  started: false,
  fields: genFields(),
  awaitingPlayer: { ...defaultPlayer, id: 2 },
  currentPlayer: { ...defaultPlayer },
  addShipsMode: null,
  showGameOverDialog: false,
  showStopGameDialog: false,
  collision: false,
};

const getNextDirection = (d?: Direction = 'E'): Direction => {
  const directions = ['S', 'W', 'N', 'E'];
  if (d === directions[directions.length - 1]) return directions[0];
  const index = directions.indexOf(d);
  return directions[index + 1];
};

const defaultAddShipsMode: AddShipsMode = {
  direction: getNextDirection(),
  ship: null,
};

const gameReducer = (
  state: GameState = initialState,
  { type, payload }: Action,
): GameState => {
  switch (type) {
    case 'ADD_SHIPS_MODE':
      return { ...state, addShipsMode: { ...defaultAddShipsMode } };
    case 'FIELD_MOUSE_OVER': {
      const { ship } = payload || {};
      const { addShipsMode } = state;
      return { ...state, addShipsMode: { ...addShipsMode, ship } };
    }
    case 'TOGGLE_DIRECTION': {
      const { direction } = state.addShipsMode || {};
      const { addShipsMode } = state;
      return {
        ...state,
        addShipsMode: {
          ...addShipsMode,
          direction: getNextDirection(direction),
        },
      };
    }
    case 'START_GAME':
      return { ...state, started: true, addShipsMode: null };
    case 'STOP_GAME':
      return { ...initialState };
    case 'COLLISION_DETECTED':
      return { ...state, collision: true };
    case 'RESET_COLLISION':
      return { ...state, collision: false };
    case 'ADD_SHIP': {
      const {
        // $FlowFixMe
        addShipsMode: { ship },
        currentPlayer,
        currentPlayer: { ships },
      } = state;
      const shipToUpdate = ships.find(s => !s.complete);

      if (!shipToUpdate) return state;

      const index = ships.indexOf(shipToUpdate);

      return {
        ...state,
        currentPlayer: {
          ...currentPlayer,
          ships: [
            ...ships.slice(0, index),
            { ...ship, complete: true },
            ...ships.slice(index + 1),
          ],
        },
      };
    }
    case 'SUCCESS': {
      const { currentPlayer, currentPlayer: { ships } } = state;
      const { shootedShip } = payload || {};
      const index = ships.indexOf(shootedShip);
      return {
        ...state,
        currentPlayer: {
          ...currentPlayer,
          ships: [
            ...ships.slice(0, index),
            { ...shootedShip, sunk: true },
            ...ships.slice(index + 1),
          ],
        },
      };
    }
    case 'MISSED': {
      const { fields } = state;
      const { field } = payload || {};
      return {
        ...state,
        fields: updateField(fields, field, { ...field, missed: true }),
      };
    }
    case 'TOGGLE_GAME_OVER_DIALOG':
      return { ...state, showGameOverDialog: !state.showGameOverDialog };
    case 'TOGGLE_STOP_GAME_DIALOG':
      return { ...state, showStopGameDialog: !state.showStopGameDialog };
    default:
      return state;
  }
};

export default gameReducer;

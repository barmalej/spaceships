// @flow
import type { Field, GameAction } from './reducer';
import type { Deps } from '../index';
import { buildShipFields } from './utils';

export const toggleDirection = (): GameAction => ({ type: 'TOGGLE_DIRECTION' });

export const fieldMouseOver = (field: Field) => ({
  getState,
}: Deps): GameAction => {
  const { currentPlayer: { ships }, addShipsMode } = getState().game;
  const ship = ships.find(s => !s.complete);

  if (!ship || !addShipsMode) return { type: 'ON_MOUSE_OVER' };

  return {
    type: 'FIELD_MOUSE_OVER',
    payload: {
      ship: {
        ...ship,
        fields: buildShipFields(ship.type, field, addShipsMode.direction),
      },
    },
  };
};

export const addShip = () => ({ getState, dispatch }: Deps): GameAction => {
  const addShipAction: GameAction = { type: 'ADD_SHIP' };
  try {
    const {
      currentPlayer: { ships },
      // $FlowFixMe
      addShipsMode: { ship },
    } = getState().game;
    const collision = ships.find(s =>
      s.fields.find(f =>
        // $FlowFixMe
        ship.fields.find(field => field.x === f.x && field.y === f.y),
      ),
    );

    if (collision) {
      setTimeout(() => dispatch({ type: 'RESET_COLLISION' }), 1500);
      return { type: 'COLLISION_DETECTED' };
    }

    if (ships.filter(s => !s.complete).length === 1) {
      dispatch((addShipAction: Object));
      return { type: 'START_GAME' };
    }
  } catch (e) {
    throw new Error('addShip action could be used only on addShipsMode');
  }

  return addShipAction;
};

export const fire = (field: Field) => ({
  getState,
  dispatch,
}: Deps): GameAction => {
  const { currentPlayer: { ships } } = getState().game;
  const shootedShip = ships.find(s =>
    s.fields.find(f => f.x === field.x && f.y === field.y),
  );

  if (!shootedShip) return { type: 'MISSED', payload: { field } };

  const successAction: GameAction = {
    type: 'SUCCESS',
    payload: { shootedShip },
  };

  if (ships.filter(s => !s.sunk).length === 1) {
    dispatch((successAction: Object));
    return { type: 'TOGGLE_GAME_OVER_DIALOG' };
  }

  return successAction;
};

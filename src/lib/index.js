// @flow
import { combineReducers, createStore, applyMiddleware } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import { createLogger } from 'redux-logger';
import app from './app/reducer';
import game from './game/reducer';
import type { AppState } from './app/reducer';
import type { GameState } from './game/reducer';

export type Action = { type: string, payload?: any };

export type State = {
  app: AppState,
  game: GameState,
};

export type Deps = {
  dispatch: (action: Action) => void,
  getState: () => State,
};

const injectMiddleware = deps => ({ dispatch, getState }) => next => action =>
  next(
    typeof action === 'function'
      ? action({ ...deps, dispatch, getState })
      : action,
  );

const reducers = combineReducers({
  app,
  game,
  routing,
});

const configureStore = () => {
  const logger = createLogger({
    collapsed: true,
  });
  const middleware = [injectMiddleware(), logger];
  return createStore(reducers, applyMiddleware(...middleware));
};

export default configureStore;

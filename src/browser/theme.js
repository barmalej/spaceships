export const green = '#3B9C3B';
export const red = '#ff3333';
export const inkViolet = '#af00ff';
export const bWidth = 2;
export const borderWidth = `${bWidth}px`;
export const borderStyle = 'solid';
export const fieldBoxShadow = '0 0 15px 1px inset';

// @flow
import React from 'react';
import { generate } from 'shortid';
import Field from './field';
import type { Field as iField } from '../../lib/game/reducer';

type Props = {| row: Array<iField> |};

const Row = ({ row }: Props) => (
  <div>
    {row.map((field: iField) => (
      <Field {...{ field, key: `field_${generate()}` }} />
    ))}
  </div>
);

export default Row;

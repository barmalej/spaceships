// @flow
import React from 'react';
import { connect } from 'react-redux';
import { generate } from 'shortid';
import Modal from 'react-modal';
import { Button } from '../components';
import Row from './row';
import type { State } from '../../lib';
import type { Field } from '../../lib/game/reducer';
import modalStyles from './modal.style';
import { inkViolet, borderWidth, borderStyle } from '../../browser/theme';

type StateProps = {|
  fields: Array<Array<Field>>,
  showGameOverDialog: boolean,
  showStopGameDialog: boolean,
|};

type DispatchProps = {|
  dispatch: Function,
|};

type Props = StateProps & DispatchProps;

const Game = ({
  fields,
  showGameOverDialog,
  showStopGameDialog,
  dispatch,
}: Props) => {
  const stopGame = () => dispatch({ type: 'STOP_GAME' });
  const toggleStopGameDialog = () =>
    dispatch({ type: 'TOGGLE_STOP_GAME_DIALOG' });
  const onModalButtonClick = (e: Event) => {
    e.preventDefault();
    dispatch({ type: 'TOGGLE_GAME_OVER_DIALOG' });
    stopGame();
  };
  const onStopOKButtonClick = (e: Event) => {
    e.preventDefault();
    toggleStopGameDialog();
    stopGame();
  };
  const onStopCancelButtonClick = (e: Event) => {
    e.preventDefault();
    toggleStopGameDialog();
  };

  const modalStyle = modalStyles();

  return (
    <div>
      <div
        style={{
          display: 'flex',
          borderLeft: `${borderWidth} ${borderStyle} ${inkViolet}`,
        }}
      >
        {fields.map((row: Array<Field>) => (
          <Row row={row} key={`row_${generate()}`} />
        ))}
      </div>
      <Modal isOpen={showGameOverDialog} style={modalStyle}>
        <h1>Game OVER!</h1>
        <p>Congrats, you won yourself!</p>
        <Button onClick={onModalButtonClick}>OK</Button>
      </Modal>
      <Modal isOpen={showStopGameDialog} style={modalStyle}>
        <h1>Stop Game!</h1>
        <p>Do you really want to stop the game?</p>
        <Button onClick={onStopOKButtonClick} danger>
          Yes
        </Button>
        <Button onClick={onStopCancelButtonClick}>No</Button>
      </Modal>
    </div>
  );
};

const mapStateProps = (state: State): StateProps => ({
  fields: state.game.fields,
  showGameOverDialog: state.game.showGameOverDialog,
  showStopGameDialog: state.game.showStopGameDialog,
});

export default connect(mapStateProps)(Game);

import { inkViolet } from '../../browser/theme';

export default function modalStyles() {
  return {
    overlay: {
      position: 'fixed',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: 'rgba(0, 0, 0, 0.75)',
    },
    content: {
      position: 'absolute',
      top: '40px',
      left: '30%',
      right: '30%',
      bottom: '70%',
      border: '1px solid #ccc',
      background: '#fff',
      overflow: 'auto',
      WebkitOverflowScrolling: 'touch',
      borderRadius: '4px',
      outline: 'none',
      padding: '20px',
      textAlign: 'center',
      color: inkViolet,
    },
  };
}

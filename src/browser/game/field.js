// @flow
import React from 'react';
import Radium from 'radium';
import { connect } from 'react-redux';
import { generate } from 'shortid';
import type {
  Field as iField,
  Player,
  Ship,
  AddShipsMode,
} from '../../lib/game/reducer';
import type { State } from '../../lib';
import { shipOnField } from '../../lib/game/utils';
import {
  addShip,
  fire,
  toggleDirection,
  fieldMouseOver,
} from '../../lib/game/actions';
import fieldStyles from './field.style';

type StateProps = {|
  ships: Array<Ship>,
  fields: Array<Array<iField>>,
  started: boolean,
  addShipsMode: ?AddShipsMode,
  player: Player,
|};

type DispatchProps = {|
  addShip: Function,
  toggleDirection: Function,
  fieldMouseOver: Function,
  fire: Function,
|};

type ComponentProps = {|
  field: iField,
|};

type Props = StateProps & DispatchProps & ComponentProps;

const Field = ({
  field,
  ships,
  started,
  addShip,
  fire,
  toggleDirection,
  addShipsMode,
  fieldMouseOver,
}: Props) => {
  const ship = shipOnField(ships, field);
  const isShip: boolean =
    !!ship ||
    (!!addShipsMode &&
      !!addShipsMode.ship &&
      !!addShipsMode.ship.fields.find(f => f.x === field.x && f.y === field.y));

  const style = fieldStyles(isShip);

  const getOnFieldClick = (field: iField) => (e: Event) => {
    e.preventDefault();
    if (addShipsMode) {
      addShip();
    } else if (started) {
      fire(field);
    }
  };

  const onContextMenu = (e: Event) => {
    e.preventDefault();
    toggleDirection();
    fieldMouseOver(field);
  };

  const onMouseOver = (e: Event) => {
    e.preventDefault();
    fieldMouseOver(field);
  };

  const onClick = getOnFieldClick(field);

  const Missed = () => <div style={style.missed} />;

  const Sunk = () => (
    <div style={style.sunk.parent}>
      <div style={style.sunk.slash} />
      <div style={style.sunk.backslash} />
    </div>
  );

  const id = generate();

  return (
    <div
      {...{
        id,
        style: style.field,
        onClick,
        onContextMenu,
        onMouseOver,
      }}
    >
      {field.missed && <Missed />}
      {(ship || {}).sunk && <Sunk />}
    </div>
  );
};

const mapProps = (state: State): StateProps => ({
  player: state.game.currentPlayer,
  fields: state.game.fields,
  ships: state.game.currentPlayer.ships,
  started: state.game.started,
  addShipsMode: state.game.addShipsMode,
});

const dispatchProps: DispatchProps = {
  addShip,
  fire,
  toggleDirection,
  fieldMouseOver,
};

export default connect(mapProps, dispatchProps)(Radium(Field));

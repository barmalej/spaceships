// @flow
import React from 'react';
import {
  inkViolet,
  bWidth,
  borderWidth,
  borderStyle,
  fieldBoxShadow,
  red,
} from '../theme';

const missedD = 10;
const dimensions = 50;
const fullDimensions = dimensions + bWidth;
const reducedDimensions = dimensions - bWidth;
const dimensionsPx = `${dimensions}px`;

type FieldStyles = {
  sunk: {
    slash: React.CSSProperties,
    backslash: React.CSSProperties,
    parent: React.CSSProperties,
  },
  missed: React.CSSProperties,
  field: React.CSSProperties,
};

export default function fieldStyles(isShip?: boolean = false): FieldStyles {
  const slashCommon = {
    borderColor: red,
    width: `${Math.sqrt(fullDimensions ** 2 + fullDimensions ** 2)}px`,
    borderBottomWidth: borderWidth,
    borderBottomStyle: borderStyle,
    borderBottomColor: red,
    position: 'relative',
  };

  return {
    sunk: {
      parent: {
        width: '100%',
        height: '100%',
      },
      slash: {
        ...slashCommon,
        transform: 'rotate(0.125turn)',
        top: `${reducedDimensions / 2}px`,
        left: `-${reducedDimensions / 4}px`,
      },
      backslash: {
        ...slashCommon,
        transform: 'rotate(-0.125turn)',
        top: `${(reducedDimensions - bWidth * 2) / 2}px`,
        left: `-${(reducedDimensions - bWidth) / 4}px`,
      },
    },
    missed: {
      backgroundColor: red,
      borderRadius: '50%',
      width: `${missedD}px`,
      height: `${missedD}px`,
      position: 'relative',
      margin: '0 auto',
      top: `${dimensions / 2 - missedD / 2}px`,
    },
    field: {
      color: inkViolet,
      lineHeight: dimensionsPx,
      backgroundColor: isShip ? inkViolet : 'white',
      width: dimensionsPx,
      height: dimensionsPx,
      borderBottomWidth: borderWidth,
      borderRightWidth: borderWidth,
      borderBottomColor: inkViolet,
      borderRightColor: inkViolet,
      borderBottomStyle: borderStyle,
      borderRightStyle: borderStyle,
      ':hover': {
        boxShadow: fieldBoxShadow,
      },
    },
  };
}

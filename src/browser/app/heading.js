import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { updateHeading } from '../../lib/app/actions';

const Heading = ({ heading }) => (
  /* eslint-disable no-unneeded-ternary */
  <div>
    <h1>{heading ? heading : 'Enter heading'}</h1>
    <ul>
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/about">About</Link>
      </li>
    </ul>
  </div>
);

Heading.propTypes = {
  heading: PropTypes.string.isRequired,
};

export default connect(
  state => ({
    heading: state.app.heading,
  }),
  { updateHeading },
)(Heading);

// @flow
import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import Home from './home';
import About from './about';

type Props = {|
  store: Object,
  history: Object,
  Router?: Object,
|};

// $FlowFixMe
const App = ({ store, history, Router }: Props) => (
  <Provider store={store}>
    <Router history={history}>
      <div style={{ fontFamily: 'StressedHand' }}>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
      </div>
    </Router>
  </Provider>
);

App.defaultProps = {
  Router: BrowserRouter,
};

export default App;

import React from 'react';
import Game from '../game';
import Menu from '../menu';
import { CenteredContainer } from '../components';

const Home = () => (
  <CenteredContainer>
    <Menu />
    <Game />
  </CenteredContainer>
);

export default Home;

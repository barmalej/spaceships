import React from 'react';
import { render } from 'react-dom';
import { createBrowserHistory } from 'history';
import { syncHistoryWithStore } from 'react-router-redux';
import Modal from 'react-modal';
import App from './app';
import configureStore from '../lib';

const appElement = document.getElementById('app');
Modal.setAppElement(appElement);

const store = configureStore();
store.dispatch({ type: 'APP_STARTED' });
const history = syncHistoryWithStore(createBrowserHistory(), store);
const props = { store, history };

render(<App {...props} />, appElement);

if (module.hot) {
  module.hot.accept('./app', () => {
    /* eslint-disable global-require */
    const NextApp = require('./app').default;
    render(<NextApp {...props} />, appElement);
  });
}

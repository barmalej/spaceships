// @flow
import React from 'react';
import { connect } from 'react-redux';
import Radium from 'radium';
import type { State } from '../../lib/index';

const scoreStyles: React.CSSProperties = {
  marginLeft: 'auto',
};

type Props = {|
  score: number,
  started: boolean,
|};

const Score = ({ score, started }: Props) =>
  started ? <div style={scoreStyles}>Sunk {score}</div> : null;

const mapStateProps = (state: State): Props => ({
  score: state.game.currentPlayer.ships.filter(s => s.sunk).length,
  started: state.game.started,
});

export default connect(mapStateProps)(Radium(Score));

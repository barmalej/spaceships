// @flow
import React from 'react';
import { inkViolet, borderWidth, borderStyle } from '../../browser/theme';

interface Styles {
  wrapper: React.CSSProperties;
  dashboard: React.CSSProperties;
  startButton: React.CSSProperties;
}

const menuStyles: Styles = {
  wrapper: {
    padding: '20px',
    borderWidth,
    borderStyle,
    color: inkViolet,
  },
  dashboard: {
    textAlign: 'left',
    display: 'flex',
  },
  startButton: {
    ':hover': {
      backgroundColor: '#ffffff',
      color: '#EEA200',
    },
  },
};

export default menuStyles;

// @flow
import React from 'react';
import Radium from 'radium';
import { connect } from 'react-redux';
import type { State } from '../../lib';
import { Button } from '../components';
import menuStyles from './menu.style';
import Score from './score';
import Info from './info';
import { red } from '../theme';

type StateProps = {|
  started: boolean,
  addShipsMode: ?Object,
|};

type DispatchProps = {|
  dispatch: Function,
|};

type Props = StateProps & DispatchProps;

const Menu = ({ dispatch, started, addShipsMode }: Props) => {
  const onClick = e => {
    e.preventDefault();
    dispatch({
      type: started ? 'TOGGLE_STOP_GAME_DIALOG' : 'ADD_SHIPS_MODE',
    });
  };

  const getButtonText = () => (started ? 'Stop' : 'Start');
  const style = started
    ? { ...menuStyles.startButton, backgroundColor: red }
    : { ...menuStyles.startButton };

  return (
    <div style={menuStyles.wrapper}>
      <h1>Battleship</h1>
      <nav style={menuStyles.dashboard}>
        <Button {...{ style, onClick, disabled: !!addShipsMode }}>
          {getButtonText()}
        </Button>
        <Info />
        <Score />
      </nav>
    </div>
  );
};

const mapStateProps = (state: State): StateProps => ({
  started: state.game.started,
  addShipsMode: state.game.addShipsMode,
});

export default connect(mapStateProps)(Radium(Menu));

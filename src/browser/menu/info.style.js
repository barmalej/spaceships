// @flow
import React from 'react';

type InfoStyles = {
  wrapper: React.CSSProperties,
  tip: React.CSSProperties,
};

const infoStyles: InfoStyles = {
  wrapper: { textAlign: 'center', width: '75%', fontFamily: 'sans-serif' },
  tip: {
    verticalAlign: 'sub',
    fontSize: '14px',
    fontFamily: 'Vernada',
  },
};

export default infoStyles;

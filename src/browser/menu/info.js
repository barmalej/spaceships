// @flow
import React from 'react';
import { connect } from 'react-redux';
import type { State } from '../../lib';
import infoStyles from './info.style';

type Props = {|
  addShipsMode: ?Object,
  collision: boolean,
|};

const Info = ({ addShipsMode, collision }: Props) =>
  addShipsMode && (
    <div style={infoStyles.wrapper}>
      <div>
        {collision ? 'Ships can not overlap' : 'Add your ships to buttlefield'}
      </div>
      <em style={infoStyles.tip}>Right click to rotate ship</em>
    </div>
  );

const mapStateProps = (state: State): Props => ({
  addShipsMode: state.game.addShipsMode,
  collision: state.game.collision,
});

export default connect(mapStateProps)(Info);

// @flow
import React from 'react';

type Props = { children: any };

const CenteredContainer = ({ children }: Props) => (
  <div style={{ textAlign: 'center' }}>
    <div style={{ display: 'inline-block' }}>{children}</div>
  </div>
);

export default CenteredContainer;

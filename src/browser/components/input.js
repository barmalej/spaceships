// @flow
import React from 'react';
import Radium from 'radium';

const inputStyles = {
  color: 'green'
};

type Props = {
  prop: string,
}

const Input = (props: Props) => (
  <div style={inputStyles}>
    <input />
  </div>
);

export default Radium(Input);

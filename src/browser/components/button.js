// @flow
import React from 'react';
import Radium from 'radium';
import styles from './button.style';

type Props = {
  tag: string,
  children: any,
  disabled: boolean,
  danger: boolean,
  style: React.CSSProperties,
};

const Button = ({
  tag: Tag = 'button',
  children,
  danger,
  ...otherProps
}: Props) => {
  const newProps = {
    ...otherProps,
    style: styles({ ...otherProps, danger }),
  };

  return <Tag {...newProps}>{children}</Tag>;
};

export default Radium(Button);

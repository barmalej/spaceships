import color from 'color';
import { green, red } from '../../browser/theme';

const styles = props => {
  const getBackgroundColor = ({ disabled, danger }) => {
    let background = green;
    if (danger) background = red;
    if (disabled) {
      background = color(background)
        .lighten(0.7)
        .hex();
    }
    return background;
  };
  const backgroundColor = getBackgroundColor(props);
  const buttonStyle = {
    padding: '10px',
    borderRadius: '3px',
    backgroundColor,
    color: 'white',
    fontSize: '16px',
    ':hover': {
      backgroundColor: color(backgroundColor)
        .darken(0.3)
        .hex(),
    },
  };

  return {
    ...buttonStyle,
    ...props.style,
  };
};

export default styles;

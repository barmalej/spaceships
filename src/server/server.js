import React from 'react';
import Koa from 'koa';
import KoaRouter from 'koa-router';
import webpack from 'webpack';
import { devMiddleware, hotMiddleware } from 'koa-webpack-middleware';
import { renderToString } from 'react-dom/server';
import { createStore } from 'redux';
// import { createBrowserHistory } from 'history';
// import { syncHistoryWithStore } from 'react-router-redux';
import { StaticRouter } from 'react-router';
import config from './webpack.config';
import App from '../browser/app';
import reducers from '../lib/index';

const compile = webpack(config);

const app = new Koa()
  .use(
    devMiddleware(compile, {
      stats: {
        colors: true,
      },
    }),
  )
  .use(
    hotMiddleware(compile, {
      log: console.log,
    }),
  );

app.use(ctx => {
  const store = createStore(reducers);
  // const history = syncHistoryWithStore(createBrowserHistory.call(), store);
  const context = {};
  const Router = props => (
    <StaticRouter location={ctx.request.url} context={context}>
      {props.children}
    </StaticRouter>
  );
  const markup = renderToString(<App {...{ store, history: {}, Router }} />);
  ctx.type = 'text/html';
  ctx.body = `<!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
        </head>
        <body>
            <div id="app">${markup}</div>
            <script>window.__INITIAL_STATE__ = ${JSON.stringify(
              store.getState(),
            )}</script>
        </body>
    </html>`;
});

export default app;

# Battle ship test task

## Spec:

1) Let's create an onscreen grid of cells aligned within a square 10 by 10.

2) Then we set up initial battle ships - one L shaped, one I shaped and two dot shaped. Initial battle ships cannot overlap.

3) Start actual game play after any kind of user input which would simulate shots at random positions - any missed shot would indicate already hit area, any shot at any of initial ships would visually indicate that battle ship has sink (change of ship color would be fine enough).

4) Program must be able to tell that all ships have sunk and game is over.

## Technologies:

- koa - serverside framework
- react - for view layer
- redux - functional implementation of FLUX architecture
- webpack - javascript module bundler 
- babel - for modern ES6 syntax support
- eslint - basic static analysis
- flow - typechecks for javascript
- radium - CSS in JS library

## Usage:

`$ yarn install` (or `npm i`)  
`$ yarn dev` (or `npm run dev`)
